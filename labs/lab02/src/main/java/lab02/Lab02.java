package lab02;

import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.time.LocalDateTime;

public class Lab02 {
    public static void main(String[] args) {
        KieServices kService = KieServices.Factory.get();
        KieContainer kContainer = kService.getKieClasspathContainer();

        execute(kService, kContainer);
    }

    public static void execute(KieServices kService, KieContainer kContainer) {
        KieSession kSession = kContainer.newKieSession("lab02-ksession");

        // kSession.setGlobal("list", new ArrayList<Object>());

        kSession.addEventListener(new DebugAgendaEventListener());
        kSession.addEventListener(new DebugRuleRuntimeEventListener());

        KieRuntimeLogger logger = kService.getLoggers().newFileLogger(kSession, "./logs");

        final Item milk = new Item("Milk", "", 100);
        final Item eggs = new Item("Eggs", "", 80);
        final Item ham = new Item("Ham", "", 130);

        final Storage storage = new Storage("main");
        storage.addItem(milk, 10);
        storage.addItem(eggs, 2);

        final Client c1 = new Client("username1");
        c1.add2Cart(milk, 1);
        c1.add2Cart(eggs, 2);

        final Client c2 = new Client("username2", false);
        c2.add2Cart(milk, 1);
        c2.add2Cart(eggs, 2);
        c2.setCoupon("COUPON");

        kSession.insert(storage);
        kSession.insert(c1);
        kSession.insert(c2);

        kSession.fireAllRules();

//        System.out.println(c1);
//        System.out.println(c2);
        System.out.println(c1.getUsername()+" : "+c1.getOrder().getDiscount());
        System.out.println(c2.getUsername()+" : "+c2.getOrder().getDiscount());

        logger.close();

    }
}
