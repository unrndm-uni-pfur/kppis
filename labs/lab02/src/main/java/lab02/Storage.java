package lab02;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private String name;
    private Map<Item, Integer> item2amount = new HashMap<>();


    public Storage(String name) {
        this.name = name;
    }

    public void addItem(Item item, Integer amount) {
        if (item2amount.containsKey(item)) {
            item2amount.put(item, item2amount.get(item)+amount);
        } else {
            item2amount.put(item, amount);
        }
    }

    public void removeItem(Item item, Integer amount) throws Exception {
        Integer num = item2amount.getOrDefault(item, 0);
        if (num < amount) {
            throw new Exception("asd");
        } else {
            item2amount.put(item, num-amount);
        }
    }

    public Integer getItemAmount(Item item) {
        return item2amount.getOrDefault(item, 0);
    }

    public Map<Item, Integer> getItems() {
        return item2amount;
    }
}
