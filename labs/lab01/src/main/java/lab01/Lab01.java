package lab01;

import java.util.ArrayList;

import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import lab01.Machine;

public class Lab01 {
    public static void main(String[] args) {
        KieServices kService = KieServices.Factory.get();
        KieContainer kContainer = kService.getKieClasspathContainer();

        execute(kService, kContainer);
    }

    public static void execute(KieServices kService, KieContainer kContainer) {
        KieSession kSession = kContainer.newKieSession("lab01-ksession");

        // kSession.setGlobal("list", new ArrayList<Object>());

        kSession.addEventListener(new DebugAgendaEventListener());
        kSession.addEventListener(new DebugRuleRuntimeEventListener());

        KieRuntimeLogger logger = kService.getLoggers().newFileLogger(kSession, "./logs");

        final Machine m1 = new Machine("Type1");
        m1.getFunctions().add("GateWay Server");
        final Machine m2 = new Machine("Type2");
        m2.getFunctions().add("DNS Server");

        kSession.insert(m1);
        kSession.insert(m2);

        kSession.fireAllRules();

        System.out.println(m1.repr());
        System.out.println(m2.repr());

        logger.close();

    }
}
