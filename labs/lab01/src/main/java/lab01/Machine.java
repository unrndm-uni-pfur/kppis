package lab01;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Collection;
import java.util.Calendar;
import java.sql.Timestamp;
import java.util.stream.Collectors;

import lab01.Test;


public class Machine {
    private String type;
    private String serialNumber;
    private Timestamp creationTs;
    private Timestamp testsDueTime;
    private List<String> functions = new ArrayList<>();
    private Collection<Test> tests = new HashSet<>();
 
    public Machine() {
        super();
        this.creationTs = new Timestamp(System.currentTimeMillis());
    }

    public Machine(String type) {
        super();
        this.creationTs = new Timestamp(System.currentTimeMillis());
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Timestamp getCreationTs() {
        return creationTs;
    }

    public void setCreationTs(Timestamp creationTs) {
        this.creationTs = creationTs;
    }

    public Timestamp getTestsDueTime() {
        return testsDueTime;
    }

    public void setTestsDueTime(Timestamp testsDueTime) {
        this.testsDueTime = testsDueTime;
    }

    public List<String> getFunctions() {
        return functions;
    }

    public void setFunctions(List<String> functions) {
        this.functions = functions;
    }

    public Collection<Test> getTests() {
        return tests;
    }

    public void setTests( Collection<Test> tests) {
        this.tests = tests;
    }

    public boolean containsTestId(Integer id) {
        return tests.stream().anyMatch(t -> t.getId().equals(id));
    }

    public boolean containsTest(Test test) {
        return tests.stream().anyMatch(t -> t.getId().equals(test.getId()));
    }

    public List<Integer> TestIds() {
        return tests.stream().mapToInt(Test::getId).distinct().boxed().collect(Collectors.toList());
    }

    public void setTestsDueTime(int numberOfDays) {
        this.setDueTime(Calendar.DATE, numberOfDays);
    }

    public void setDueTime(int field, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.getCreationTs());
        calendar.add(field, amount);
        this.setTestsDueTime(new Timestamp(calendar.getTimeInMillis()));
    }

    public String repr() {
        return this+
            "\ntype: "+type+
            "\nfunctions: "+functions+
            "\ntests: "+tests.stream().mapToInt(Test::getId).distinct().boxed().collect(Collectors.toList())+
            "\ntestsDueTime: "+testsDueTime;
    }


}
    
